# py-fthat

Python client to communicate with a fthat on a raspberry pi.

See <https://tx-pi.de/de/hat/>

It uses the ``gpiozerro`` library.
