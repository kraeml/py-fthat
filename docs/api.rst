py-fthat Python API
===================

Getting started with py-fthat is easy.
The main class you need to care about is :class:`~fthat.ftpi.BaseFtTxPiHat`

fthat.ftpi
------------

.. automodule:: fthat.ftpi
   :members:
