.. py-fthat documentation master file, created by
   sphinx-quickstart on Tue Mar 22 11:02:00 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to py-fthat's documentation!
====================================

.. toctree::
   :maxdepth: 2
   :caption: User Guide

   README

.. toctree::
   :maxdepth: 2
   :caption: Programmer Reference
   
   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
